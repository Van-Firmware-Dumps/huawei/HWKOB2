#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from a53_a53_32 device
$(call inherit-product, device/unknown/a53_a53_32/device.mk)

PRODUCT_DEVICE := a53_a53_32
PRODUCT_NAME := lineage_a53_a53_32
PRODUCT_BRAND := Huawei
PRODUCT_MODEL := a53_a53_32
PRODUCT_MANUFACTURER := unknown

PRODUCT_GMS_CLIENTID_BASE := android-unknown

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="a53_a53_32-user 10 QP1A.190711.020 eng.root.20211103.020939 dev-keys"

BUILD_FINGERPRINT := Huawei/a53_a53_32/a53_a53_32:10/QP1A.190711.020/root202111030209:user/dev-keys

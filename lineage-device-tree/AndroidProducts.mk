#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_a53_a53_32.mk

COMMON_LUNCH_CHOICES := \
    lineage_a53_a53_32-user \
    lineage_a53_a53_32-userdebug \
    lineage_a53_a53_32-eng
